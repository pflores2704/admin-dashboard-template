import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Nav } from '../'

const Layout: React.FunctionComponent<{}> = ({ children }) => {
  const { pathname } = useRouter();
  const menu = [
    { name: 'Home', url: '/' },
    { name: 'Users', url: '/users' },
    { name: 'Settings', url: '/settings'},
  ];

  return (
    <div>
      <Nav />
      <main className="flex">
        <aside className="bg-white p-4 pt-20 w-1/5 border-r">
          <h1 className="text-indigo-600 font-bold uppercase text-xs mb-4">
            Menu
          </h1>
          <ul className="pr-4 text-sm text-gray-700">
            {
              menu.map(item => (
                <li key={item.name + item.url} className="mt-2">
                  <Link href={item.url}>
                    <a href="#" className={`block px-4 py-1.5 rounded transition-colors ${item.url === pathname ? 'font-semibold bg-blue-100 rounded text-indigo-500 border-l-4 border-indigo-500' : 'hover:bg-gray-200 hover:text-indigo-500'}`}>
                      {item.name}
                    </a>
                  </Link>
                </li>
              ))
            }
          </ul>
        </aside>

        <section className="flex-grow bg-gray-100 h-screen px-8 pt-16">
          {children}
        </section>
      </main>
    </div>
  );
}

export default Layout;