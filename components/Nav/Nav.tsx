import { supabase } from "../../utils/supabaseClient"

function Nav() {
    const handleLogout = async () => {
        let { error } = await supabase.auth.signOut()
    }

    return (
        <nav className="absolute left-0 right-0 bg-white p-2 shadow-md">
            <div className="flex max-h-10 items-center overflow-hidden">
                <div className="flex-1">
                    <h4 className="font-bold text-indigo-700">Admin Panel</h4>
                </div>
                <div className="flex-1">
                    <div className="flex h-full flex-row-reverse items-center">
                        <div className="relative h-10 w-10 cursor-pointer rounded-full bg-blue-200">
                            <img
                                onClick={() => handleLogout()}
                                src="https://i.pravatar.cc/80"
                                alt=""
                                className="rounded-full"
                            />
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    )
}

export default Nav
