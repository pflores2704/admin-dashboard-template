import React, { useState } from "react"
import { supabase } from "../utils/supabaseClient"

export default function Auth() {
    const [loading, setLoading] = useState<boolean>(false)
    const [email, setEmail] = useState<string>("")

    const handleLogin = async (email: string) => {
        try {
            setLoading(true)
            const { error } = await supabase.auth.signIn({ email })

            if (error) throw error

            alert("Check your email for the login link!")
        } catch (error) {
            alert(error.error_description || error.message)
        } finally {
            setLoading(false)
        }
    }

    const handleSubmit = (event: React.SyntheticEvent) => {
        event.preventDefault()
        console.log("Submitted form.")
        handleLogin(email)
    }

    return (
        <div className="-mt-40 rounded bg-white p-6 shadow-xl md:w-1/3 lg:w-1/4">
            <h1 className="mt-2 mb-6 text-center text-xl font-bold capitalize text-indigo-600">
                Admin panel
            </h1>
            <p className="text-center text-lg font-semibold">
                Log in by entering your email address.
            </p>
            <p className="text-center text-sm text-gray-600">
                We will send you an email with a link to login.
            </p>
            <form className="mt-6">
                <div>
                    <label className="my-2 block text-xs font-semibold uppercase">
                        Email
                    </label>
                    <input
                        type="email"
                        onInput={(e) => setEmail(e.currentTarget.value)}
                        className="w-full rounded border py-2 px-4 shadow"
                    />
                </div>
                <div className="-mx-6 my-6 flex flex-col border-b px-6 pb-10 text-center">
                    <button
                        type="submit"
                        onClick={handleSubmit}
                        className="rounded bg-green-500 p-2 text-sm font-semibold text-white hover:bg-green-400 disabled:opacity-50"
                    >
                        Login
                    </button>
                </div>
            </form>
            <div className="-m-6 rounded rounded-tl-none rounded-tr-none bg-indigo-600 p-6">
                <p className="text-center text-xs text-white">
                    Don't have an account?&nbsp;
                    <a href="#" className="underline">
                        Create an account
                    </a>
                    .
                </p>
            </div>
        </div>
    )
}
