import React from 'react';

interface IProps {
  title?: string;
}

const Card: React.FunctionComponent<IProps> = ({ title, children }) => (
  <div className="bg-white rounded shadow-md mt-6">
    {title && (
      <h1 className="border-b border-gray-200 px-6 py-4 capitalize text-gray-400 font-semibold">
        {title}
      </h1>
    )}
    <div className="px-6 py-4">
      {children}
    </div>
  </div>
);

export default Card;