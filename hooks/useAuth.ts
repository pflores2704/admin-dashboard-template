interface Auth {
  isAuthenticated: boolean;
  user: {};
}

function useAuth(): Auth  {

  return {
    isAuthenticated: !true,
    user: {},
  }
}

export default useAuth;