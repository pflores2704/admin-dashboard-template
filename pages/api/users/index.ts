import { client } from './../../../modules/axios';
import crypto from 'crypto';

const uuid = require('uuid');

export default async (req, res) => {
  const { method } = req;

  switch(method) {
    case 'GET':
      try {
        const data = await getAllUsers(req, res);
        res.statusCode = 200
        res.json(data);
      } catch (e) {
        res.statusCode = 500;
        res.json({ error: e });
      }
      break;
    case 'POST':
      try {
        const newUser = await createNewUser(req, res);
        res.json({ message: "User created successfully", id: newUser });
      } catch (e) {
        res.statusCode = 500;
        res.json({ error: e });
      }
      break;
    default:
      res.statusCode = 405
      res.json({ message: 'Method not allowed' });
  }
}

const getAllUsers = async (req, res) => {
  const query = `
    {
      allUsers {
        data {
          _id
          email
          username
          firstName
          lastName
          role
        }
      }
    }
  `
  const { data: { allUsers } }: any = await client.post({ query });
  return allUsers.data
}

const createNewUser = async (req, res) => {
  const { body } = req;
  const { email, password, role } = body;

  // 1. Validate all required fields exists
  if (!email || !password || !role) {
    res.statusCode = 400;
    res.json({ message: "Failed to create new user" });
  }


  try {
    // 2. Create salt and hash password
    const salt = crypto.createHash('sha256')
      .update(uuid.v4(), "utf8")
      .digest("hex");

    const hashedPassword = crypto.createHash('sha256')
      .update(password, "utf8")
      .update(salt)
      .digest("hex");


    // 3. Build GraphQL mutation
    const query = `
      mutation {
        createUser(data: {
          email: "${email}",
          password: "${hashedPassword}",
          role: ${role},
          salt: "${salt}"
        }) {
          _id
        }
      }
    `;

    // 4. Post the query to db
    const { data: { createUser } } = await client.post({ query });

    return createUser._id;
  } catch (e) {
    throw e;
  }
}