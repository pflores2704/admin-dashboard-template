import { Request, Response } from 'express';
import { client } from '../../../modules/axios';

export default async (req: Request, res: Response) => {
  const { userId } = req.query;

  const query: any = `
    {
      findUserByID(id: "${userId}") {
        _id
        email
        username
        firstName
        lastName
        role
      }
    }
  `;

  try {
    const { data: response = {} } = await client.post({ query });

    if (!response.findUserByID)
      return res.status(404).send({ status: 404, message: 'Not found' });

    res.json(response.findUserByID);
  } catch(e) {
    res.json(e);
  }
}