import { client } from './../../modules/axios';

export default async (req, res) => {
  const { method } = req;

  switch(method) {
    case 'POST':
      return handlePostSessionRequest(req, res);
    case 'GET':
      return handleGetSessionRequest(req, res);
    default:
      res.statusCode = 405;
      res.json({ status: 405, message: 'Method not allowed' });
  }
}

async function handlePostSessionRequest(req, res) {
  res.statusCode = 200;
  return res.json({ message: 'Session created' })
};

async function handleGetSessionRequest(req, res) {
  res.statusCode = 200;
  return res.json({ current: {}, previous: [] });
};