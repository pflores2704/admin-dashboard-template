import { useEffect, useState } from 'react'
import Head from 'next/head'
import Link from 'next/link'
import useSWR from 'swr'
import { Layout, Card } from '../../components'

const fetcher = (args) => fetch(args).then(res => res.json())

export default function Users() {
  const [loading, setLoading] = useState<boolean>(true)
  const { data = [], error } = useSWR('/api/users', fetcher)
  
  useEffect(() => {
    if (data) {
      setLoading(false)
    }
  }, [data])

  const TableItem = ({ even, children, className = '' }) => (
    <div className={`table-cell px-4 py-4 ${even ? 'bg-gray-50' : ''} ${className}`}>
      {children}
    </div>
  )
  
  return (
    <Layout>
      <Head>
        <title>Users</title>
      </Head>
      <h1 className="title">
        Users
      </h1>
      <Card>
        {loading && 'Fetching users...'}
        <div className="table w-full">
          <div className="table-header-group">
            <div className="table-row text-indigo-600 font-bold text-sm">
              <div className="table-cell px-4 pb-4">E-mail</div>
              <div className="table-cell px-4 pb-4">Username</div>
              <div className="table-cell px-4 pb-4">Role</div>
              <div className="table-cell px-4 pb-4 text-right">Actions</div>
            </div>
          </div>
          {
            data.length > 0 && (
              data.map((user, index) => (
                <div key={user._id} className="table-row text-sm hover:bg-indigo-50">
                  <TableItem even={index % 2} className="rounded-l-md">
                    {user.email}
                  </TableItem>
                  <TableItem even={index % 2}>
                    {user.username || '-'}
                  </TableItem>
                  <TableItem even={index % 2}>
                    {user.role}
                  </TableItem>
                  <TableItem even={index % 2} className="rounded-r-md text-right">
                    <Link href={`/users/${encodeURIComponent(user._id)}`}>
                      <button>Edit</button>
                    </Link>
                  </TableItem>
                </div>
              ))
            )
          }
        </div>
        {error && <p>{error}</p>}
      </Card>
    </Layout>
  )
}