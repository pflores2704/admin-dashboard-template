import { useRouter } from 'next/router'
import Link from 'next/link'
import useSWR from 'swr'
import { Card, Layout } from '../../components'

const fetcher = (args) => fetch(args).then(res => res.json())

export default function UserDetails() {
  const router = useRouter()
  const { userId } = router.query
  const { data = {}, error } = useSWR(`/api/users/${userId}`, fetcher)

  return (
    <Layout>
      <h1 className="title">User Details</h1>

      {
        data && (
          <Card>
            <Link href={'/users'} >
              <span className="cursor-pointer text-gray-500 font-semibold">&lsaquo; Back</span>
            </Link>
            <div className="flex flex-col space-y-4 mx-auto max-w-xs">
              <div className='bg-indigo-300 rounded p-2'>
                <img className='mx-auto' src="https://via.placeholder.com/150" alt="" />
              </div>
              <div className='bg-indigo-300 rounded p-2'>
                <input value={data.email} />
              </div>
              <div className='bg-indigo-300 rounded p-2'>
                <input value={data.username} placeholder='Username'/>
              </div>
              <div className='bg-indigo-300 rounded p-2'>
                <input value={data.role} />
              </div>
            </div>
          </Card>
        )
      }
    </Layout>
  )
}