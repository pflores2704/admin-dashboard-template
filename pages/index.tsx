import Head from "next/head"
import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import { Layout, Card } from "./../components"
import { supabase } from "../utils/supabaseClient"

export default function Home() {
    const [session, setSession] = useState(undefined)
    const router = useRouter()

    useEffect(() => {
        const session = supabase.auth.session()
        setSession(session)

        supabase.auth.onAuthStateChange((_event, session) => {
            setSession(session)
        })
    }, [])

    useEffect(() => {
        if (session === null) {
            router.push("/login")
        }
    }, [session])

    return (
        <Layout>
            <Head>
                <title>Home</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <h1 className="title">Home</h1>
            <Card>My Card</Card>
        </Layout>
    )
}
