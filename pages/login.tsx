import Head from "next/head"
import { Auth } from "./../components"

export default function Login() {
    return (
        <div className="flex h-screen items-center justify-center bg-gray-100">
            <Head>
                <title>Login</title>
            </Head>
            <Auth />
        </div>
    )
}
