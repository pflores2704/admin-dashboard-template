import axios from 'axios';

const _client = axios.create({
  baseURL: process.env.DB_ENDPOINT,
  method: 'post',
  headers: {
    "Content-Type": "application/json",
    "Authorization": `Bearer ${process.env.DB_KEY}`
  }
});

class CustomClient {

  async post(data) {
    try {
      const res = await _client.post('', data);
      return res.data;
    } catch(e) {
      throw e;
    }
  }

}

const client = new CustomClient();

export { client };